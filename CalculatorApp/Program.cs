﻿namespace Calculator
{

    internal class Calculator
    {
        static void Main()
        {
            double? result;
            int attempts = 3;
            char? selectedOperation = null;


            while (attempts > 0)
            {
                Console.Write("Please, choose the operation: +, -, / or * ");
                var operatortToCheck = Console.ReadLine();

                if (operatortToCheck?.Length == 1 && "+-*/".Contains(operatortToCheck[0]))
                {
                    selectedOperation = operatortToCheck[0];
                    break; // Exit the loop if the operator is entered correctly
                }
                else
                {
                    Console.WriteLine("Invalid operator entered. Try again.");
                    attempts--;
                }
            }

            if (attempts == 0)
            {
                Console.WriteLine("End of the program.");
                Console.ReadLine();
                return;
            }

            Console.WriteLine("Please, enter the number 1:");
            string operand1 = Console.ReadLine();

            Console.WriteLine("Please, enter the number 2:");
            string operand2 = Console.ReadLine();


            if (double.TryParse(operand1, out double num1) && double.TryParse(operand2, out double num2))
            {
                switch (selectedOperation)
                {
                    case '+':
                        result = calculatorOperations.Addition(num1, num2);
                        Console.WriteLine("Result: " + result);
                        break;

                    case '-':
                        result = calculatorOperations.Substraction(num1, num2);
                        Console.WriteLine("Result: " + result);
                        break;

                    case '*':
                        result = calculatorOperations.Multiplication(num1, num2);
                        Console.WriteLine("Result: " + result);
                        break;

                    case '/':
                        try
                        {
                            result = calculatorOperations.Division(num1, num2);
                            Console.WriteLine("Result: " + result);
                        }
                        catch (DivideByZeroException e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        break;

                    default:
                        Console.WriteLine("Impossible to divide by zero");
                        break;
                }
            }

            else
            {
                Console.WriteLine("Incorrect numbers are entered");
            }

            Console.ReadKey();
        }
    }
}

