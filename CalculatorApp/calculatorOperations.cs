﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    internal class calculatorOperations
    {


        public static double Addition(double num1, double num2)
        {
            return num1 + num2;
        }


        public static double Substraction(double num1, double num2)
        {
            return num1 - num2;
        }


        public static double Multiplication(double num1, double num2)
        {
            return num1 * num2;
        }

        public static double Division(double num1, double num2)
        {
            if (num2 != 0)
            {
                return num1 / num2;
            }
            else 
            {
                throw new DivideByZeroException("Impossible to divide by zero");
            }
            
        }
    }
}
