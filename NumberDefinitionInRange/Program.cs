﻿namespace NumberDefinitionInRange
{
    class Program
    {
        static void Main()
        {
            int attempts = 3;

            Console.WriteLine("Enter a number from 0 to 100: ");

            // Read user input and try to parse it as an integer
            while (attempts > 0)
            {
                if (int.TryParse(Console.ReadLine(), out int userNumber))
                {
                    if (userNumber >= 0 && userNumber <= 14)
                    {
                        Console.WriteLine("The number is in the range [0 - 14].");
                        break;
                    }
                    else if (userNumber >= 15 && userNumber <= 35)
                    {
                        Console.WriteLine("The number is in the range [15 - 35].");
                        break;
                    }
                    else if (userNumber >= 36 && userNumber <= 49)
                    {
                        Console.WriteLine("The number is in the range [36 - 49].");
                        break;
                    }
                    else if (userNumber >= 50 && userNumber <= 100)
                    {
                        Console.WriteLine("The number is in the range [50 - 100].");
                        break;
                    }
                    else
                    {
                        Console.WriteLine("The number does not belong to any of the specified ranges.");
                        break;
                    }
                }
                else
                {
                    if (attempts == 1)
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Error: Please enter a valid integer.");
                        attempts--;
                    }
                }
                
            }
            Console.ReadKey();
        }
    }
}