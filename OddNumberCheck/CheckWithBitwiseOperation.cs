﻿using System;
namespace OddNumberCheck
{
    class CheckWithBitwiseOperation
    {
        public void CheckWithBitwiseOperator()
        {
            Console.Write("Enter a number: ");
            string enteredValue = Console.ReadLine();   

            if (int.TryParse(enteredValue, out int num))
            {
                if ((num & 1) == 0)
                {
                    Console.WriteLine("The number is even.");
                }
                else
                {
                    Console.WriteLine("The number is odd.");
                }
            }
            else
            {
                Console.WriteLine("Invalid input.");
            }

            Console.WriteLine("Press enter.");
            Console.ReadKey();
        }
    }
}

