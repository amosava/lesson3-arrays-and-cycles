﻿using OddNumberCheck;

class Program
{
    static void Main()
    {
        // first check option
        CheckOddWithOperator operatorCheck = new CheckOddWithOperator();

        operatorCheck.CheckWithOperator();

        //second check option
        CheckWithBitwiseOperation bitwiseCheck = new CheckWithBitwiseOperation();
        bitwiseCheck.CheckWithBitwiseOperator();

    }
}