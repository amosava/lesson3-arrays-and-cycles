﻿using System;
using System.Collections.Generic;

class Program
{
    static void Main()
    {
        // Dictionary with translations
        Dictionary<string, string> weatherConditionTranslations = new Dictionary<string, string>()
        {
            { "солнечно", "sunny" },
            { "дождь", "rain" },
            { "ветренно", "windy" },
            { "облачно", "cloudy" },
            { "тепло", "warm" },
            { "морозно", "frosty" },
            { "гроза", "storm" },
            { "снег", "snow" },
            { "туман", "fog" },
            { "град", "hail" }
        };

        while (true)
        {
            Console.Write("Enter a word in Russian (or \"exit\" to complete): ");
            string enteredWord = Console.ReadLine().ToLower();

            if (enteredWord == "exit")
                break;

            if (weatherConditionTranslations.ContainsKey(enteredWord))
            {
                string translationFromRussian = weatherConditionTranslations[enteredWord];
                Console.WriteLine($"Translation: {translationFromRussian}");
            }
            else
            {
                Console.WriteLine("This word is absent in the dictionary.");
            }
        }

        Console.ReadKey();
    }
}
